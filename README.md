No âmbito da UC de Laboratório Multimédia 2, e em articulação com a UC de Guionismo, foi proposto como projeto final o desenvolvimento de uma aplicação multimédia sendo a sua temática de livre escolha.

Para o nosso projeto decidimos criar uma aplicação cujo tópico será a Física Teórica, tendo como sub-tópico as leis físicas de Newton.