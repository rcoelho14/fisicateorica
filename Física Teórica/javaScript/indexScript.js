/**
 * Created by helia on 6/22/2017.
 */
/**----------------------------------------------ABOUT AND CONTACTS-------------------------------------------------*/
var aboutAnchor = document.getElementById('sobre');
var showAbout = document.getElementById('relativeAbout');
var xButtonAbout = document.getElementById('xButtonAbout');

aboutAnchor.onclick = function() {
    showAbout.style.visibility = 'visible';
    showContact.style.visibility = 'hidden';
};

xButtonAbout.onclick = function() {
    showAbout.style.visibility = 'hidden';
};

var contactAnchor = document.getElementById('contactos');
var showContact = document.getElementById('relativeContact');
var xButtonContact = document.getElementById('xButtonContact');

contactAnchor.onclick = function() {
    showContact.style.visibility = 'visible';
    showAbout.style.visibility = 'hidden';
};

xButtonContact.onclick = function() {
    showContact.style.visibility = 'hidden';
};