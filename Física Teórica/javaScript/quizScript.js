/**
 * Created by helia on 6/8/2017.
 */

//--------------------------------ARRAY COM TODAS AS PERGUNTAS; OPCOES E RESPECTIVAS RESPOSTAS--------------------------------------------------------//


var questionsGroup = [
    {   question: "O que é uma força?",
        options: ["É uma interação entre corpos.", "O movimento efetuado por um corpo.", "Um objeto em repouso."],
        correct: "É uma interação entre corpos."
    },
    {
        question: "Qual a primeira Lei de Newton?",
        options: ["Princípio Fundamental da Dinâmica.", "Princípio da Ação e Reação.", "Princípio da Inércia."],
        correct: "Princípio da Inércia."
    },
    {
        question: "O que é o princípio da ação reação?",
        options: ["A Força é sempre diretamente proporcional ao produto da aceleração de um corpo pela sua massa.", "Um corpo em repouso tende a permanecer em repouso, e um corpo em movimento tende a permanecer em movimento.", "As forças atuam sempre em pares, para toda força de ação, existe uma força de reação."],
        correct: "As forças atuam sempre em pares, para toda força de ação, existe uma força de reação."
    },
    {
        question: "O que é a Termodinâmica?",
        options: ["É o ramo da física que estuda as leis que regem as relações entre trabalho, calor e energia térmica.", "É o estudo da interação entre corpos.", "É o ramo da física que estuda as leis das interações elétricas a nível atómico."],
        correct: "É o ramo da física que estuda as leis que regem as relações entre trabalho, calor e energia térmica."
    },
    {
        question: "O que rege o calor?",
        options: ["O inverso de frio.", "As radiações solares.", "A quantidade de interações moleculares."],
        correct: "A quantidade de interações moleculares."
    },
    {
        question: "O que define uma escala de temperaturas?",
        options: ["As forças eletromagnéticas.", "As interações entre corpos.", "O equilíbrio térmico entre corpos."],
        correct: "O equilíbrio térmico entre corpos."
    },
    {
        question: "Qual destas afirmações é verdadeira?",
        options: ["A quantidade de entropia de qualquer sistema isolado termodinamicamente tende a diminuir com o tempo, até alcançar um valor mínimo.", "Quanto maior a energia interna de um sistema, maior será seu potencial para a realização de trabalho.", "As diferenças entre sistemas em contato tendem a diferenciar-se."],
        correct: "Quanto maior a energia interna de um sistema, maior será seu potencial para a realização de trabalho."
    },
    {
        question: "O que descreve PV=nRT?",
        options: ["A relação entre a pressão, volume e temperatura.", "A relação entre o trabalho, força e movimento.", "A relação entre a eletricidade, a corrente e as forças."],
        correct: "A relação entre a pressão, volume e temperatura."
    },
    {
        question: "Qual das leis diz “quando um sistema se aproxima da temperatura do zero absoluto, todos os processos cessam, e a entropia tem um valor mínimo”?",
        options: ["Primeira Lei da Termodinâmica.", "Segunda Lei da Termodinâmica.", "Terceira Lei da Termodinâmica."],
        correct: "Terceira Lei da Termodinâmica."
    },
    {
        question: "O que é um campo eletromagnético?",
        options: ["É resultado do movimento de cargas elétricas.", "É um corpo que emite eletricidade.", "É a força criada pela resistência elétrica"],
        correct: "É resultado do movimento de cargas elétricas."
    },
    {
        question: "O que define uma carga elétrica?",
        options: ["É uma propriedade física fundamental que determina as interações eletromagnéticas.", "É a quantidade de eletricidade existente num sistema.", "É a força de interação entre a eletricidade e a gravidade."],
        correct: "É uma propriedade física fundamental que determina as interações eletromagnéticas."
    },
    {
        question: "Quando é que um corpo é neutro eletricamente?",
        options: ["Quando ele tem pH de 7.", "Quando há tal igualdade ou equilíbrio de cargas num corpo.", "Quando não tem movimento relativo."],
        correct: "Quando há tal igualdade ou equilíbrio de cargas num corpo."
    },
    {
        question: "O que estabelece a Lei de Coulomb?",
        options: ["A quantidade de entropia de qualquer sistema isolado termodinamicamente tende a incrementar-se com o tempo, até alcançar um valor máximo.", "Um objeto que está em repouso ficará em repouso a não ser que uma força resultante não nula aja sobre ele.", "A força de atração ou repulsão entre dois corpos carregados é diretamente proporcional ao produto de suas cargas e inversamente proporcional ao quadrado da distância."],
        correct: "A força de atração ou repulsão entre dois corpos carregados é diretamente proporcional ao produto de suas cargas e inversamente proporcional ao quadrado da distância."
    },
    {
        question: "Qual destes afirmações é verdadeira?",
        options: ["A força elétrica entre duas cargas com o mesmo sinal é repulsiva.", "A força elétrica entre duas cargas com o mesmo sinal é atrativa.", "A força elétrica entre duas cargas com sinais diferentes é repulsiva"],
        correct: "A força elétrica entre duas cargas com o mesmo sinal é repulsiva."
    },
    {
        question: "Qual destes é verdadeiro?",
        options: ["Um objeto que está em movimento retilíneo uniforme não mudará a sua velocidade a não ser que uma força resultante não nula aja sobre ele.", "Um objeto que está em movimento retilíneo uniforme irá mudar a sua velocidade a não ser que uma força resultante não nula aja sobre ele.", "Um objeto nunca poderá ester num movimento retilíneo uniforme."],
        correct: "Um objeto que está em movimento retilíneo uniforme não mudará a sua velocidade a não ser que uma força resultante não nula aja sobre ele."
    },
    {
        question: "O que afirma o princípio fundamental da dinâmica?",
        options: ["Um objeto que está em repouso ficará em repouso a não ser que uma força resultante não nula aja sobre ele.", "Um objeto que está em movimento retilíneo uniforme não mudará a sua velocidade a não ser que uma força resultante não nula aja sobre ele.", "Afirma que a força resultante  F→ em uma partícula é igual à taxa temporal de variação do seu momento linear F→ em um sistema de referência inercial."],
        correct: "Afirma que a força resultante  F→ em uma partícula é igual à taxa temporal de variação do seu momento linear F→ em um sistema de referência inercial."
    },
    {
        question: "O que gera um campo eletromagnético?",
        options: ["A variação de um campo elétrico gera um campo magnético.", "A variação da entropia.", "As diferenças entre sistemas eletromagnéticos."],
        correct: "A variação de um campo elétrico gera um campo magnético."
    }];

//--------------------------------------------------------------------------------------------------------------------//
//---------------------PARA SAIR UM NUMERO ALEATORIO PARA RELACIONAR COM A POSICAO DO ARRAY---------------------------//
    var random1 = Math.floor((Math.random() * 17));
    var firstPosition = document.getElementById("questionA");
    var printA='';
    firstPosition.setAttribute('class', 'question');
    firstPosition.setAttribute('data-source', questionsGroup[random1].correct);
    firstPosition.innerHTML = questionsGroup[random1].question;

    for(var a=0; a<1; a++){
        printA+= questionsGroup[random1].question+'<br>';
        printA+='<ul>'+
            '<li><input type="radio" name="radio1' + a + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random1].options[0]+'</li>'+
            '<li><input type="radio" name="radio1' + a + '" />'+'&nbsp;&nbsp;'+questionsGroup[random1].options[1]+'</li>'+
            '<li><input type="radio" name="radio1' + a + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random1].options[2]+'</li>'+
            '</ul><br>';
    }
    document.getElementById("questionA").innerHTML = printA;

    questionsGroup.splice(random1, 1);

//------------------PERGUNTA PARA A SEGUNDA POSICAO---------------------------------------------------------------------
    var random2 = Math.floor((Math.random() * 16));
        var secondPosition = document.getElementById("questionB");
        var printB='';
        secondPosition.setAttribute('class', 'question');
        secondPosition.setAttribute('data-source', questionsGroup[random2].correct);
        secondPosition.innerHTML = questionsGroup[random2].question;
        for(var b=0; b<1; b++){
            printB+= questionsGroup[random2].question+'<br>';
            printB+='<ul>'+
                '<li><input type="radio" name="radio2' + b + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random2].options[0]+'</li>'+
                '<li><input type="radio" name="radio2' + b + '" />'+'&nbsp;&nbsp;'+questionsGroup[random2].options[1]+'</li>'+
                '<li><input type="radio" name="radio2' + b + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random2].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionB").innerHTML = printB;

    questionsGroup.splice(random2,1);

//------------------PERGUNTA PARA A TERCEIRA POSICAO---------------------------------------------------------------------
    var random3 = Math.floor((Math.random() * 15));
    var thirdPosition = document.getElementById("questionC");
    var printC='';
    thirdPosition.setAttribute('class', 'question');
    thirdPosition.setAttribute('data-source', questionsGroup[random3].correct);
    thirdPosition.innerHTML = questionsGroup[random3].question;
        for(var c=0; c<1; c++){
            printC+= questionsGroup[random3].question+'<br>';
            printC+='<ul>'+
                '<li><input type="radio" name="radio3' + c + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random3].options[0]+'</li>'+
                '<li><input type="radio" name="radio3' + c + '" />'+'&nbsp;&nbsp;'+questionsGroup[random3].options[1]+'</li>'+
                '<li><input type="radio" name="radio3' + c + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random3].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionC").innerHTML = printC;

    questionsGroup.splice(random3,1);

//------------------PERGUNTA PARA A QUARTA POSICAO---------------------------------------------------------------------
    var random4 = Math.floor((Math.random() * 14));
    var fourthPosition = document.getElementById("questionD");
    var printD='';
    fourthPosition.setAttribute('class', 'question');
    fourthPosition.setAttribute('data-source', questionsGroup[random4].correct);
    fourthPosition.innerHTML = questionsGroup[random4].question;
        for(var d=0; d<1; d++){
            printD+= questionsGroup[random4].question+'<br>';
            printD+='<ul>'+
                '<li><input type="radio" name="radio4' + d + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random4].options[0]+'</li>'+
                '<li><input type="radio" name="radio4' + d + '" />'+'&nbsp;&nbsp;'+questionsGroup[random4].options[1]+'</li>'+
                '<li><input type="radio" name="radio4' + d + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random4].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionD").innerHTML = printD;

    questionsGroup.splice(random4,1);

//------------------PERGUNTA PARA A QUINTA POSICAO---------------------------------------------------------------------
    var random5 = Math.floor((Math.random() * 13));
    var fifthPosition = document.getElementById("questionE");
    var printE='';
    fifthPosition.setAttribute('class', 'question');
    fifthPosition.setAttribute('data-source', questionsGroup[random5].correct);
    fifthPosition.innerHTML = questionsGroup[random5].question;
        for(var e=0; e<1; e++){
            printE+= questionsGroup[random5].question+'<br>';
            printE+='<ul>'+
                '<li><input type="radio" name="radio5' + e + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random5].options[0]+'</li>'+
                '<li><input type="radio" name="radio5' + e + '" />'+'&nbsp;&nbsp;'+questionsGroup[random5].options[1]+'</li>'+
                '<li><input type="radio" name="radio5' + e + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random5].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionE").innerHTML = printE;

    questionsGroup.splice(random5,1);

//------------------PERGUNTA PARA A SEXTA POSICAO---------------------------------------------------------------------
    var random6 = Math.floor((Math.random() * 12));
    var sixthPosition = document.getElementById("questionF");
    var printF='';
    sixthPosition.setAttribute('class', 'question');
    sixthPosition.setAttribute('data-source', questionsGroup[random6].correct);
    sixthPosition.innerHTML = questionsGroup[random6].question;
        for(var f=0; f<1; f++){
            printF+= questionsGroup[random6].question+'<br>';
            printF+='<ul>'+
                '<li><input type="radio" name="radio6' + f + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random6].options[0]+'</li>'+
                '<li><input type="radio" name="radio6' + f + '" />'+'&nbsp;&nbsp;'+questionsGroup[random6].options[1]+'</li>'+
                '<li><input type="radio" name="radio6' + f + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random6].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionF").innerHTML = printF;

    questionsGroup.splice(random6,1);

//------------------PERGUNTA PARA A SETIMA POSICAO---------------------------------------------------------------------
    var random7 = Math.floor((Math.random() * 11));
    var seventhPosition = document.getElementById("questionG");
    var printG='';
    seventhPosition.setAttribute('class', 'question');
    seventhPosition.setAttribute('data-source', questionsGroup[random7].correct);
    seventhPosition.innerHTML = questionsGroup[random7].question;
        for(var g=0; g<1; g++){
            printG+= questionsGroup[random7].question+'<br>';
            printG+='<ul>'+
                '<li><input type="radio" name="radio7' + g + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random7].options[0]+'</li>'+
                '<li><input type="radio" name="radio7' + g + '" />'+'&nbsp;&nbsp;'+questionsGroup[random7].options[1]+'</li>'+
                '<li><input type="radio" name="radio7' + g + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random7].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionG").innerHTML = printG;

    questionsGroup.splice(random7,1);

//------------------PERGUNTA PARA A OITAVA POSICAO---------------------------------------------------------------------
    var random8 = Math.floor((Math.random() * 10));
    var eighthPosition = document.getElementById("questionH");
    var printH='';
    eighthPosition.setAttribute('class', 'question');
    eighthPosition.setAttribute('data-source', questionsGroup[random8].correct);
    eighthPosition.innerHTML = questionsGroup[random8].question;
        for(var h=0; h<1; h++){
            printH+= questionsGroup[random8].question+'<br>';
            printH+='<ul>'+
                '<li><input type="radio" name="radio8' + h + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random8].options[0]+'</li>'+
                '<li><input type="radio" name="radio8' + h + '" />'+'&nbsp;&nbsp;'+questionsGroup[random8].options[1]+'</li>'+
                '<li><input type="radio" name="radio8' + h + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random8].options[2]+'</li>'+
                '</ul><br>';
        }
        document.getElementById("questionH").innerHTML = printH;

    questionsGroup.splice(random8,1);

//------------------PERGUNTA PARA A NONA POSICAO---------------------------------------------------------------------
    var random9 = Math.floor((Math.random() * 9));
    var ninethPosition = document.getElementById("questionI");
    var printI='';
    ninethPosition.setAttribute('class', 'question');
    ninethPosition.setAttribute('data-source', questionsGroup[random9].correct);
    ninethPosition.innerHTML = questionsGroup[random9].question;
        for(var i=0; i<1; i++){
            printI+= questionsGroup[random9].question+'<br>';
            printI+='<ul>'+
            '<li><input type="radio" name="radio9' + i + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random9].options[0]+'</li>'+
            '<li><input type="radio" name="radio9' + i + '" />'+'&nbsp;&nbsp;'+questionsGroup[random9].options[1]+'</li>'+
            '<li><input type="radio" name="radio9' + i + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random9].options[2]+'</li>'+
            '</ul><br>';
    }
    document.getElementById("questionI").innerHTML = printI;

    questionsGroup.splice(random9,1);

//------------------PERGUNTA PARA A DECIMA POSICAO---------------------------------------------------------------------
    var random10 = Math.floor((Math.random() * 8));
    var tenthPosition = document.getElementById("questionJ");
    var printJ='';
    tenthPosition.setAttribute('class', 'question');
    tenthPosition.setAttribute('data-source', questionsGroup[random10].correct);
    tenthPosition.innerHTML = questionsGroup[random10].question;
        for(var j=0; j<1; j++){
        printJ+= questionsGroup[random10].question+'<br>';
        printJ+='<ul>'+
            '<li><input type="radio" name="radio10' + j + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random10].options[0]+'</li>'+
            '<li><input type="radio" name="radio10' + j + '" />'+'&nbsp;&nbsp;'+questionsGroup[random10].options[1]+'</li>'+
            '<li><input type="radio" name="radio10' + j + '"/>'+'&nbsp;&nbsp;'+questionsGroup[random10].options[2]+'</li>'+
            '</ul><br>';
        }
        document.getElementById("questionJ").innerHTML = printJ;

    questionsGroup.splice(random10,1);




//----------------------------------------------------------------------------------------------------------------------
//-----------------------PINTAR A VERDE AS QUE ESTIVEREM CORRECTAS / VERMELHO AS ERRADAS--------------------------------
//----------------------------------------------------------------------------------------------------------------------

    var submit = document.getElementById("submitQuestions");
    var tryOneMoreTime = document.getElementById("tryAgain");
    submit.innerHTML = '<input type="button" value="Ver Resultado">';
    tryOneMoreTime.innerHTML = '<input type="button" value="Tentar Novamente">';
    var printResult = document.getElementById("result");

    submit.onclick = function() {
        var questions = document.getElementsByClassName('question');
        var cont=0;

        for (var l = 0; l < questions.length; l += 1) {
            var question = $(questions[l]);
            var answer = question.find('input[type=radio]:checked').parent().text().trim();
            var inspect = question.attr('data-source');
            var listItem = question.find('input[type=radio]:checked').parent();
            if(answer === inspect){
                cont++;
                listItem.addClass('printGreen');
            } else{
                listItem.addClass('printRed');
            }

        }
        if(cont === 0){
            printResult.innerHTML = "Acertou 0% das perguntas!";
        } else {
            printResult.innerHTML = "Acertou " + cont + "0% das perguntas!";
        }

        $( ":radio" ).css('visibility', 'hidden');
    };

    tryOneMoreTime.onclick = function() {
        location.reload();
    };
