/**
 * Created by helia on 6/14/2017.
 */



/**-------------------------------------------------PARALLAX SCROLL--------------------------------------------------*/

function  parallax() {
    var earth = document.getElementById('earth');
    earth.style.top = 250 -(window.scrollY/4)+'px';
    var pluto = document.getElementById('pluto');
    pluto.style.bottom = 20 +(window.scrollY/30)+'px';
    var venus = document.getElementById('venus');
    venus.style.top = 900 -(window.scrollY/10)+'px';
    venus.style.left = 10 +(window.scrollY/50)+'px';
    var saturn = document.getElementById('saturn');
    saturn.style.top = 100 -(window.scrollY/40)+'px';
    saturn.style.left = 500 -(window.scrollY/40)+'px';
    var neptune = document.getElementById('neptune');
    neptune.style.bottom = 500 -(window.scrollY/45)+'px';
    var jupiter = document.getElementById('jupiter');
    jupiter.style.bottom = -175 +(window.scrollY/70)+'px';

}
window.addEventListener("scroll", parallax, false);




