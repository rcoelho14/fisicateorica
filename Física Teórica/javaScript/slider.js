
/** ---------------Dropdown submenu-------------------**/
$(document).ready(function(){
    $('.dropdown-submenu a.test').on("click", function(e){
        $(this).next('ul').toggle();
        e.stopPropagation();
        e.preventDefault();
    });
});


/**----------------------------------------------ABOUT AND CONTACTS-------------------------------------------------*/
var aboutAnchor = document.getElementById('sobre');
var showAbout = document.getElementById('relativeAbout');
var xButtonAbout = document.getElementById('xButtonAbout');

aboutAnchor.onclick = function() {
    showAbout.style.visibility = 'visible';
    showContact.style.visibility = 'hidden';
};

xButtonAbout.onclick = function() {
    showAbout.style.visibility = 'hidden';
};

var contactAnchor = document.getElementById('contactos');
var showContact = document.getElementById('relativeContact');
var xButtonContact = document.getElementById('xButtonContact');

contactAnchor.onclick = function() {
    showContact.style.visibility = 'visible';
    showAbout.style.visibility = 'hidden';
};

xButtonContact.onclick = function() {
    showContact.style.visibility = 'hidden';
};





/* ____________________________________________________________________SLIDER DE CONTEÚDO___________________________________________________________________ */

var slideIndex_btn = 1;
showDivs_btn(slideIndex_btn);


function current_btn(n) {
    showDivs_btn(slideIndex_btn = n);
}

function showDivs_btn(n) {
    var i_btn;
    var x_btn = document.getElementsByClassName("slider_content_btn");
    var main_content = document.getElementsByClassName("content_cont");
    if (n > x_btn.length) {slideIndex_btn = 1}
    if (n < 1) {slideIndex_btn = x_btn.length}
    for (i_btn = 0; i_btn < x_btn.length; i_btn++) {
        x_btn[i_btn].style.display = "none";
    }
    for (i_btn = 0; i_btn < main_content.length; i_btn++) {
        main_content[i_btn].className = main_content[i_btn].className.replace(" buttons_active_main", "");
    }
    x_btn[slideIndex_btn-1].style.display = "block";
    main_content[slideIndex_btn-1].className += " buttons_active_main";
}



/*________________________________________________________  MECANICA  __________________________________________________ */

/* ________________________________________________________Primeira Lei_________________________________________________ */

var slideIndex = 1;
showDivs(slideIndex);


function currentDiv(n) {
    showDivs(slideIndex = n);
}

function showDivs(n) {
    var i;
    var x = document.getElementsByClassName("content");
    var dots = document.getElementsByClassName("plei");
    if (n > x.length) {slideIndex = 1}
    if (n < 1) {slideIndex = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" buttons_active", "");
    }
    x[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " buttons_active";
}


/* _________________________________________________________Segunda Lei______________________________________________  */



var slideIndex_sl = 1;
showDivs_sl(slideIndex_sl);


function currentDiv_sl(n) {
    showDivs_sl(slideIndex_sl = n);
}

function showDivs_sl(n) {
    var i_sl;
    var x_sl = document.getElementsByClassName("content_sl");
    var sec_law = document.getElementsByClassName("slei");
    if (n > x_sl.length) {slideIndex_sl = 1}
    if (n < 1) {slideIndex_sl = x_sl.length}
    for (i_sl = 0; i_sl < x_sl.length; i_sl++) {
        x_sl[i_sl].style.display = "none";
    }
    for (i_sl = 0; i_sl < sec_law.length; i_sl++) {
        sec_law[i_sl].className = sec_law[i_sl].className.replace(" buttons_active_sl", "");
    }
    x_sl[slideIndex_sl-1].style.display = "block";
    sec_law[slideIndex_sl-1].className += " buttons_active_sl";
}


/* ___________________________________________________________Terceira Lei_____________________________________________ */


var slideIndex_tl = 1;
showDivs_tl(slideIndex_tl);


function currentDiv_tl(n) {
    showDivs_tl(slideIndex_tl = n);
}

function showDivs_tl(n) {
    var i_tl;
    var x_tl = document.getElementsByClassName("content_tl");
    var thi_law = document.getElementsByClassName("tlei");
    if (n > x_tl.length) {slideIndex_tl = 1}
    if (n < 1) {slideIndex_tl = x_tl.length}
    for (i_tl = 0; i_tl < x_tl.length; i_tl++) {
        x_tl[i_tl].style.display = "none";
    }
    for (i_tl = 0; i_tl < thi_law.length; i_tl++) {
        thi_law[i_tl].className = thi_law[i_tl].className.replace(" buttons_active_tl", "");
    }
    x_tl[slideIndex_tl-1].style.display = "block";
    thi_law[slideIndex_tl-1].className += " buttons_active_tl";
}





/*________________________________________________________  TERMODINAMICA  __________________________________________________ */


/* ________________________________________________________Lei Zero da Termodinamica_________________________________________________ */

var slideIndex_t_z = 1;
showDivs_t_z(slideIndex_t_z);


function currentDiv_t_z(n) {
    showDivs_t_z(slideIndex_t_z = n);
}

function showDivs_t_z(n) {
    var i_t_z;
    var x_t_z = document.getElementsByClassName("content_t_z");
    var first_t_zermo = document.getElementsByClassName("zlei_t");
    if (n > x_t_z.length) {slideIndex_t_z = 1}
    if (n < 1) {slideIndex_t_z = x_t_z.length}
    for (i_t_z = 0; i_t_z < x_t_z.length; i_t_z++) {
        x_t_z[i_t_z].style.display = "none";
    }
    for (i_t_z = 0; i_t_z < first_t_zermo.length; i_t_z++) {
        first_t_zermo[i_t_z].className = first_t_zermo[i_t_z].className.replace(" buttons_active_t_z", "");
    }
    x_t_z[slideIndex_t_z-1].style.display = "block";
    first_t_zermo[slideIndex_t_z-1].className += " buttons_active_t_z";
}




/* ________________________________________________________Primeira Lei Termodinamica_________________________________________________ */

var slideIndex_t = 1;
showDivs_t(slideIndex_t);


function currentDiv_t(n) {
    showDivs_t(slideIndex_t = n);
}

function showDivs_t(n) {
    var i_t;
    var x_t = document.getElementsByClassName("content_t");
    var first_termo = document.getElementsByClassName("plei_t");
    if (n > x_t.length) {slideIndex_t = 1}
    if (n < 1) {slideIndex_t = x_t.length}
    for (i_t = 0; i_t < x_t.length; i_t++) {
        x_t[i_t].style.display = "none";
    }
    for (i_t = 0; i_t < first_termo.length; i_t++) {
        first_termo[i_t].className = first_termo[i_t].className.replace(" buttons_active_t", "");
    }
    x_t[slideIndex_t-1].style.display = "block";
    first_termo[slideIndex_t-1].className += " buttons_active_t";
}


/* _________________________________________________________Segunda Lei Termodinamica______________________________________________  */



var slideIndex_sl_t = 1;
showDivs_sl_t(slideIndex_sl_t);


function currentDiv_sl_t(n) {
    showDivs_sl_t(slideIndex_sl_t = n);
}

function showDivs_sl_t(n) {
    var i_sl_t;
    var x_sl_t = document.getElementsByClassName("content_sl_t");
    var sec_termo = document.getElementsByClassName("slei_t");
    if (n > x_sl_t.length) {slideIndex_sl_t = 1}
    if (n < 1) {slideIndex_sl_t = x_sl_t.length}
    for (i_sl_t = 0; i_sl_t < x_sl_t.length; i_sl_t++) {
        x_sl_t[i_sl_t].style.display = "none";
    }
    for (i_sl_t = 0; i_sl_t < sec_termo.length; i_sl_t++) {
        sec_termo[i_sl_t].className = sec_termo[i_sl_t].className.replace(" buttons_active_sl_t", "");
    }
    x_sl_t[slideIndex_sl_t-1].style.display = "block";
    sec_termo[slideIndex_sl_t-1].className += " buttons_active_sl_t";
}


/* ___________________________________________________________Terceira Lei_Termodinamica_____________________________________________ */


var slideIndex_tl_t = 1;
showDivs_tl_t(slideIndex_tl_t);


function currentDiv_tl_t(n) {
    showDivs_tl_t(slideIndex_tl_t = n);
}

function showDivs_tl_t(n) {
    var i_tl_t;
    var x_tl_t = document.getElementsByClassName("content_tl_t");
    var thi_termo = document.getElementsByClassName("tlei_t");
    if (n > x_tl_t.length) {slideIndex_tl_t = 1}
    if (n < 1) {slideIndex_tl_t = x_tl_t.length}
    for (i_tl_t = 0; i_tl_t < x_tl_t.length; i_tl_t++) {
        x_tl_t[i_tl_t].style.display = "none";
    }
    for (i_tl_t = 0; i_tl_t < thi_termo.length; i_tl_t++) {
        thi_termo[i_tl_t].className = thi_termo[i_tl_t].className.replace(" buttons_active_tl_t", "");
    }
    x_tl_t[slideIndex_tl_t-1].style.display = "block";
    thi_termo[slideIndex_tl_t-1].className += " buttons_active_tl_t";
}




/*________________________________________________________________________ELECTROMAGNETISMO_____________________________________________________________________*/

/* ____________________________________________________________________Força Electromagnética___________________________________________________________________ */

var slideIndex_elec = 1;
showDivs_elec(slideIndex_elec);


function currentDiv_elec(n) {
    showDivs_elec(slideIndex_elec = n);
}

function showDivs_elec(n) {
    var i_elec;
    var x_elec = document.getElementsByClassName("content_elec");
    var first_elec = document.getElementsByClassName("plei_elec");
    if (n > x_elec.length) {slideIndex_elec = 1}
    if (n < 1) {slideIndex_elec = x_elec.length}
    for (i_elec = 0; i_elec < x_elec.length; i_elec++) {
        x_elec[i_elec].style.display = "none";
    }
    for (i_elec = 0; i_elec < first_elec.length; i_elec++) {
        first_elec[i_elec].className = first_elec[i_elec].className.replace(" buttons_active_elec", "");
    }
    x_elec[slideIndex_elec-1].style.display = "block";
    first_elec[slideIndex_elec-1].className += " buttons_active_elec";
}


/* _______________________________________________________________________________Lei de Coulomb________________________________________________________________  */



var slideIndex_sl_elec = 1;
showDivs_sl_elec(slideIndex_sl_elec);


function currentDiv_sl_elec(n) {
    showDivs_sl_elec(slideIndex_sl_elec = n);
}

function showDivs_sl_elec(n) {
    var i_sl_elec;
    var x_sl_elec = document.getElementsByClassName("content_sl_elec");
    var sec_elec = document.getElementsByClassName("slei_elec");
    if (n > x_sl_elec.length) {slideIndex_sl_elec = 1}
    if (n < 1) {slideIndex_sl_elec = x_sl_elec.length}
    for (i_sl_elec = 0; i_sl_elec < x_sl_elec.length; i_sl_elec++) {
        x_sl_elec[i_sl_elec].style.display = "none";
    }
    for (i_sl_elec = 0; i_sl_elec < sec_elec.length; i_sl_elec++) {
        sec_elec[i_sl_elec].className = sec_elec[i_sl_elec].className.replace(" buttons_active_sl_elec", "");
    }
    x_sl_elec[slideIndex_sl_elec-1].style.display = "block";
    sec_elec[slideIndex_sl_elec-1].className += " buttons_active_sl_elec";
}


/* _________________________________________________________________________________________Força Entre Cargas___________________________________________________________________ */


var slideIndex_tl_elec = 1;
showDivs_tl_elec(slideIndex_tl_elec);


function currentDiv_tl_elec(n) {
    showDivs_tl_elec(slideIndex_tl_elec = n);
}

function showDivs_tl_elec(n) {
    var i_tl_elec;
    var x_tl_elec = document.getElementsByClassName("content_tl_elec");
    var thi_elec = document.getElementsByClassName("tlei_elec");
    if (n > x_tl_elec.length) {slideIndex_tl_elec = 1}
    if (n < 1) {slideIndex_tl_elec = x_tl_elec.length}
    for (i_tl_elec = 0; i_tl_elec < x_tl_elec.length; i_tl_elec++) {
        x_tl_elec[i_tl_elec].style.display = "none";
    }
    for (i_tl_elec = 0; i_tl_elec < thi_elec.length; i_tl_elec++) {
        thi_elec[i_tl_elec].className = thi_elec[i_tl_elec].className.replace(" buttons_active_tl_elec", "");
    }
    x_tl_elec[slideIndex_tl_elec-1].style.display = "block";
    thi_elec[slideIndex_tl_elec-1].className += " buttons_active_tl_elec";
}





